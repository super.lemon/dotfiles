#!/usr/bin/env bash
full_path=$(realpath $0)
dir_path=$(dirname $full_path)
home_path=$dir_path/home

function copy_configs__(){
    cp -ir $home_path/.config $HOME/
    cp -ir $home_path/.bashrc $HOME/
}

copy_configs__