if status is-interactive
    # Commands to run in interactive sessions can go here
end

if test -d ~/.config/fish/.rc
    for rc in (ls ~/.config/fish/.rc)
        source ~/.config/fish/.rc/$rc
    end
end